Examples for the Loom language
==============================

Start by installing dependencies:
```sh
npm install
```

Build all examples:
```sh
npm run build
```

Build a particular example (e.g. `tasks`):
```sh
npm run build:tasks
```

Open an example (e.g. `tasks`):
```sh
npm run open:tasks
```
